# A website about Pine Hill

## Source code
- https://gitlab.com/xurizaemon/pine-hill

## References
- https://box464.com/posts/eleventy-fetch-marvel/

## Sources
### Barfoot & Thompson
- https://www.barfoot.co.nz/
- No Dunedin results at present
- https://www.barfoot.co.nz/properties/residential/suburb=dunedin

### One Agency
- https://oneagency.nz/
- Uses Box and Dice Real Estate CRM
- AJAX paging on front page with filter, doesn't really limit to Dunedin properties.
- `curl 'https://oneagency.nz/ajax-listings?offset_val=450&type=Buy' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/113.0' -H 'Accept: */*' -H 'Accept-Language: en-US,en;q=0.5' -H 'Accept-Encoding: gzip, deflate, br' -H 'Referer: https://oneagency.nz/listings?type=Buy&town=DUNEDIN' -H 'X-Requested-With: XMLHttpRequest' -H 'Connection: keep-alive' -H 'Cookie: _gs=797b09b78388524e2a2f81bf44a0af8107fa672d' -H 'Sec-Fetch-Dest: empty' -H 'Sec-Fetch-Mode: cors' -H 'Sec-Fetch-Site: same-origin' -H 'DNT: 1' -H 'Sec-GPC: 1' -H 'TE: trailers'`
