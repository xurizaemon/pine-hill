<!doctype html>
<html lang="{{ metadata.language }}">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>{{ title or metadata.title }}</title>
		<meta name="description" content="{{ description or metadata.description }}">

		{#- Atom and JSON feeds included by default #}
		<link rel="alternate" href="/feed/feed.xml" type="application/atom+xml" title="{{ metadata.title }}">
		<link rel="alternate" href="/feed/feed.json" type="application/json" title="{{ metadata.title }}">

		<meta name="generator" content="{{ eleventy.generator }}">

    	<link rel="stylesheet" type="text/css" href="/main.css" />
        <link rel="stylesheet" type="text/css" href="/styles/prism-solarizedlight.min.css" />
        <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v2.14.1/mapbox-gl.js'></script>
        <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v2.14.1/mapbox-gl.css' rel='stylesheet' />

		{#-
		CSS bundles are provided via the `eleventy-plugin-bundle` plugin:
		1. You can add to them using `{% css %}`
		2. You can get from them using `{% getBundle "css" %}` or `{% getBundleFileUrl "css" %}`
		3. You can do the same for JS: {% js %}{% endjs %} and <script>{% getBundle "js" %}</script>
		4. Learn more: https://github.com/11ty/eleventy-plugin-bundle
		#}
	</head>
	<body class="m-8">
		<a href="#skip" class="sr-only">Skip to main content</a>

		<header class="container relative mx-auto flex">
			<div class="site-name w-1/4">
			    <a href="/" class="home-link">{{ metadata.title }}</a>
            </div>

            <nav aria-label="primary" class="flex space-x-6 w-3/4">
				{% for item in navigation.items %}
					<a href="{{ item.url }}" class="text-gray-800 dark:text-yellow hover:text-blue-500">{{ item.text }}</a>
				{% endfor %}
            </nav>
		</header>

		<main id="skip" class="container relative mx-auto">
			{{ content | safe }}
		</main>

		<footer>
		</footer>

		<!-- Current page: {{ page.url | htmlBaseUrl }} -->
	</body>
</html>
