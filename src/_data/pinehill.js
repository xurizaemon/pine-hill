let listings = [
    {
        lat: -45.842349,
        lng: 170.51711,
        markerHide: false,
        name: 'Secret forest',
        color: '#ff6666'
    },
    {
        lat: -45.83018680135179,
        lng: 170.5311102853134,
        markerHide: false,
        name: 'Spooky house',
        color: '#66ff66'
    }
];

module.exports = async () => {
    return listings;
};
