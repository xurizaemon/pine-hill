const EleventyFetch = require("@11ty/eleventy-fetch");
const jsdom = require("jsdom");
const { JSDOM } = jsdom;

let url = 'https://oneagency.nz/listings?type=Buy&town=DUNEDIN';

module.exports = async () => {
    let listings = [];
    let html = await EleventyFetch(url, {
        duration: "1d",
        type: "html"
    });
    const dom = new JSDOM(html, {
        url: "https://oneagency.nz/"
    });
    const items = dom.window.document.querySelectorAll('.index-item');
    items.forEach(async (item) => {
        let itemDescription = item.querySelector('img').alt;
        let itemUrl = item.querySelector('a').href;
        let itemHtml = await EleventyFetch(itemUrl, {
            duration: '1d',
            type: 'html',
        });
        const itemDom = new JSDOM(itemHtml, {
            url: itemUrl,
            runScripts: "dangerously"
        });
        let listing = itemDom.window.listing;
        listing.name = `<a href="${itemUrl}">${itemDescription}</a>`;
        listing.icon = 'https://assets.boxdice.com.au/one-agency/attachments/111/471/favicon_32x32.png?5ae0d9eb7cf7ee98c94f06c036c8bef3';
        listing.color = '#f47d30';
        // console.log(listing, `listing for ${itemUrl}`);
        listings.push(listing);
        console.log(listings, 'listings2');
    });
    console.log(listings, 'listings1');
    return listings;
};
