const fs = require("fs");
const parse = require('csv-parse/sync').parse;
const eleventyNavigationPlugin = require("@11ty/eleventy-navigation");
const { EleventyHtmlBasePlugin } = require("@11ty/eleventy");
const pluginTailwindCSS = require("eleventy-plugin-tailwindcss");
const { DateTime } = require("luxon");
const util = require('util');
const syntaxHighlight = require("@11ty/eleventy-plugin-syntaxhighlight");
const eleventyIgnorePlugin = require("eleventy-plugin-ignore");
const pluginRss = require('@11ty/eleventy-plugin-rss');

module.exports = (eleventyConfig) => {
  // Dev server config.
  eleventyConfig.setServerOptions({
    port: 80,
    showAllHosts: true
  });

  // Watch targets.
  eleventyConfig.addWatchTarget('./tailwind.config.js');
  eleventyConfig.addWatchTarget('./src/styles/main.css');
  eleventyConfig.addWatchTarget('./public/main.css');

  // Pass thru copies.
  eleventyConfig.addPassthroughCopy("src/media");
  eleventyConfig.addPassthroughCopy('./src/styles/prism-solarizedlight.min.css');

  eleventyConfig.addPlugin(pluginTailwindCSS, {
    src: "src/styles/main.css",
    dest: ".",
    keepFolderStructure: false,
    minify: false
  });
  eleventyConfig.addPlugin(eleventyNavigationPlugin);
  eleventyConfig.addPlugin(EleventyHtmlBasePlugin);
  eleventyConfig.addPlugin(syntaxHighlight);
  eleventyConfig.addPlugin(pluginRss);

  eleventyConfig.addFilter('json', (...args) => {
    return JSON.stringify(args);
  });


  return {
    dir: {
      input: "src",
      output: "public",
      includes: "_includes",
      layouts: "_layouts"
    }
  }
};
